/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        // button
        button_client: "#396543,",
        button_therapist: "#0b586f",
        button_disabled: "#d9d9d9",
        button_second_tap: "#999999",
        button_selected: "#fff000",
        // text
        text_dark_green: "#396543",
        text_black: "#3C3C3C",
        text_white: "#fff",
        //bg
        bg_app: "#f7f8f3",
        bg_green: "#B8C4A4",
        // width - height
        72: "4.5rem",
        74: "4.625rem",
        244: "15.25rem",
      },
    },
  },
  plugins: [],
};
