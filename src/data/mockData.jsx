import the1 from "../assets/1.png";
import the2 from "../assets/2.png";
import the3 from "../assets/3.png";
import the4 from "../assets/4.png";
import the5 from "../assets/5.png";
import the6 from "../assets/6.png";
import the7 from "../assets/7.png";
import the8 from "../assets/8.png";
import mock2 from "../assets/mock2.png";
import mock3 from "../assets/mock3.png";
import mock4 from "../assets/mock4.png";
import mock5 from "../assets/mock5.png";
import logo from "../assets/logo_img.png";

export const therapistMock = [
  {
    id: 1,
    image: the1,
  },
  {
    id: 2,
    image: the2,
  },
  {
    id: 3,
    image: the3,
  },
  {
    id: 4,
    image: the4,
  },
  {
    id: 5,
    image: the5,
  },
  {
    id: 6,
    image: the6,
  },
  {
    id: 7,
    image: the7,
  },
  {
    id: 8,
    image: the8,
  },
];

export const howtoData = [
  {
    id: 2,
    title: "เลือกหัวข้อ",
    ic: logo,
    desc: "เลือกหัวข้อที่ต้องการปรึกษา เพื่อให้ได้พบนักจิตวิทยาที่ถนัด ในการให้คำปรึกษาในหัวข้อนั้นๆ",
    img: mock2,
  },
  {
    id: 3,
    title: "นัดหมายกับนักจิตวิทยา",
    ic: logo,
    desc: "เลือกนักจิตวิทยาที่ไว้วางใจ เพื่อนัดหมายการรับคำปรึกษาล่วงหน้า",
    img: mock3,
  },
  {
    id: 4,
    title: "รับคำปรึกษา",
    ic: logo,
    desc: "พบนักจิตวิทยา เพื่อรับคำปรึกษา โดยหลังจากจบการพูดคุยจะมีบันทึก การปรึกษาให้คุณได้อ่านทบทวน",
    img: mock4,
  },
  {
    id: 5,
    title: "แบ่งปันดอกไม้ในใจคุณ",
    ic: logo,
    desc: "การพูดคุยของคุณอาจมีประโยชน์ ต่อผู้อื่นด้วย หากคุณอนุญาต เราขอแชร์Quote บางส่วนลงสวนดอกไม้ในเว็บไซต์นี้",
    img: mock5,
  },
];
