import logo from "../assets/logo_img.png";
import download_ic from "../assets/download_white.svg";
import { HashLink as Link } from "react-router-hash-link";

const Header = () => {
  return (
    <div className="flex justify-between items-center w-full px-16 py-4 z-50 bg-bg_app">
      <img src={logo} alt="logo" className="h-10 w-74" />

      <div className="flex text-text_black gap-8">
        <Link
          smooth
          to="#main"
          className="hover:scale-105 hover:duration-700 ease-in-out"
          activeStyle={"text-bg_green"}
        >
          หน้าหลัก
        </Link>
        <Link
          smooth
          to="#service"
          className="hover:scale-105 hover:duration-700 ease-in-out"
        >
          บริการของเรา
        </Link>
        <Link
          smooth
          to="#garden"
          className="hover:scale-105 hover:duration-700 ease-in-out"
        >
          สวนมายด์บลูม
        </Link>
        <Link
          smooth
          to="#therapist"
          className="hover:scale-105 hover:duration-700 ease-in-out"
        >
          นักจิตวิทยา
        </Link>
        <Link
          smooth
          to="#howto"
          className="hover:scale-105 hover:duration-700 ease-in-out"
        >
          การใช้งานมายด์บลูม
        </Link>
        <Link
          smooth
          to="#footer"
          className="hover:scale-105 hover:duration-700 ease-in-out"
        >
          ติดต่อเรา
        </Link>
      </div>
      <div>
        <button className="flex bg-text_dark_green text-text_white px-4 py-1 rounded-3xl items-center gap-1 hover:scale-105 hover:duration-700 ease-in-out">
          <span>ปรึกษาเลย</span>
          <img src={download_ic} alt="download" className="w-4 h-4" />
        </button>
      </div>
    </div>
  );
};

export default Header;
