import { Carousel, Divider, Image } from "antd";
import { HashLink as Link } from "react-router-hash-link";
import { motion } from "framer-motion";

import "./App.css";
import "./fonts/LINESeedSansTH_A_Th.ttf";
import Header from "./component/Header";
import { howtoData, therapistMock } from "./data/mockData";
import logo from "./assets/logo_img.png";
import banner from "./assets/banner.png";
import download_ic from "./assets/download_white.svg";
import download_b_ic from "./assets/download.svg";
import a_store from "./assets/app_store_badge.svg";
import g_store from "./assets/google_play_badge.svg";
import bel from "./assets/bel.png";
import ar from "./assets/arrow_r.svg";
import al from "./assets/arrow_l.svg";
import val1 from "./assets/value1.png";
import val2 from "./assets/value2.png";
import val3 from "./assets/value3.png";
import flower from "./assets/flower_ic.svg";
import mock1 from "./assets/mock1.png";
import g_ic from "./assets/google-play-icon.svg";
import a_ic from "./assets/Apple.svg";
import l_ic from "./assets/line.svg";
import f_ic from "./assets/facebook.svg";
import app_icon from "./assets/app_icon.png";
import { BrowserRouter } from "react-router-dom";

function App() {
  const onChange = (currentSlide) => {
    console.log(currentSlide);
  };

  return (
    <>
      <BrowserRouter>
        <Header />
        <motion.section
          initial={{ opacity: 0, scale: 0.8 }}
          whileInView={{ opacity: 1, scale: 1 }}
          exit={{ opacity: 0 }}
          transition={{
            type: "spring",
            damping: 10,
            mass: 0.75,
            stiffness: 100,
          }}
          className="relative h-3/5 overflow-y-hidden"
          id="main"
        >
          <h1 className="title absolute px-24 text-center w-full mt-12">
            พื้นที่ปลอดภัยสำหรับดูแลจิตใจ
          </h1>
          <img
            src={banner}
            alt="banner"
            className="w-screen h-screen object-fill pr-12"
          />
          <div className="absolute bottom-10 right-60 top-2/3 -translate-y-1/2 flex flex-col gap-4 justify-center items-center">
            <span className="rounded-3xl border border-text_dark_green bg-text_white flex justify-center w-max px-4 py-1">
              พร้อมจะผลิบานไปกับเรา
            </span>
            <button className="flex w-56 bg-text_dark_green text-text_white px-4 py-2 rounded-3xl items-center gap-1 border border-text_white text-2xl justify-center hover:scale-105 hover:duration-700 ease-in-out">
              <span>ดาวน์โหลดเลย</span>
              <img src={download_ic} alt="download" className="w-4 h-4" />
            </button>
            <button className="h-20 w-56 rounded-3xl flex flex-1 hover:scale-105 hover:duration-700 ease-in-out">
              <img src={a_store} alt="app store" className="w-full h-full" />
            </button>
            <button className="h-20 w-56 rounded-3xl flex flex-1 hover:scale-105 hover:duration-700 ease-in-out">
              <img src={g_store} alt="app store" className="w-full h-full" />
            </button>
          </div>
        </motion.section>
        <motion.section
          initial={{ opacity: 0, scale: 0.8 }}
          whileInView={{ opacity: 1, scale: 1 }}
          exit={{ opacity: 0 }}
          transition={{
            type: "spring",
            damping: 10,
            mass: 0.75,
            stiffness: 100,
          }}
          className="flex flex-col px-80 gap-8 items-center overflow-y-hidden"
          id="service"
        >
          <h1 className="title text-center w-full mt-12">เราเชื่อว่า</h1>
          <Image
            src={bel}
            alt="therappist"
            preview={false}
            className="w-full h-full p-12"
          />
          <button className="rounded-3xl border border-text_dark_green bg-text_white flex justify-center w-max px-4 py-1 items-center gap-2 hover:scale-105 hover:duration-700 ease-in-out">
            <span>มายด์บลูมใช้งานอย่างไร</span>{" "}
            <img src={ar} alt="arrow" className="w-4 h-4" />
          </button>
        </motion.section>
        <section
          className="bg-[url('./assets/sizes.png')] bg-cover flex flex-col gap-4 items-center mt-12"
          id="garden"
        >
          <h1 className="title text-center w-full mt-12">คุณค่าของเรา</h1>
          <span className="text-center">
            มายด์บลูม
            เราไม่ได้เป็นเพียงสิ่งแวดล้อมที่เหมาะสมที่ส่งเสริมให้ดอกไม้ทุกดอกผ่านพ้นข้อจำกัด{" "}
            <br />
            และกลับงอกงามอีกครั้งในแบบของตัวเอง แต่ยังเป็นพื้นที่ที่จะแบ่งปัน
            <br />
            และส่งต่อความสดใส รวมถึงพลังบวกให้กับคนอื่น ๆ ต่อไป
          </span>
          <div className="flex gap-24 justify-center items-center w-full px-16">
            <img src={val1} alt="image" className="w-64 h-full" />
            <img src={val2} alt="image" className="w-64 h-full" />
            <img src={val3} alt="image" className="w-64 h-full" />
          </div>
        </section>
        <div className="bg-text_white pb-16 pt-24">
          <motion.section
            initial={{ opacity: 0, scale: 0.8 }}
            whileInView={{ opacity: 1, scale: 1 }}
            exit={{ opacity: 0 }}
            transition={{
              type: "spring",
              damping: 10,
              mass: 0.75,
              stiffness: 100,
            }}
            className="overflow-y-hidden bg-text_white h-screen  m-0"
            id="therapist"
          >
            <div className="bg-[url('./assets/therapist.png')] bg-contain bg-no-repeat bg-center w-full h-full flex flex-col gap-4 items-center">
              <h1 className="title text-center w-full mt-12">นักจิตวิทยา</h1>
              <span className="text-center text-2xl font-semibold text-text_dark_green">
                “สิ่งแวดล้อมที่เหมาะสมที่จะทำให้คุณกลับมาเติบโตและงอกงาม”
              </span>
              <span className="text-center  text-text_dark_green">
                ดำเนินการตามมาตรฐานจรรยาบรรณด้านจิตวิทยา <br />
                เพื่อให้มั่นใจได้ว่าทุกเรื่องราว ของคุณจะถูกเก็บเป็นความลับ{" "}
                <br />
                นำทีมโดยนักจิตวิทยาคลินิกที่มีประสบการณ์ด้านการดูแลสุขภาพใจมาแล้วกว่า
                15 ปี{" "}
              </span>
              <div className="flex items-center gap-2 bg-text_white px-4 rounded-3xl text-text_black">
                <img src={flower} alt="flower" />
                <span className="text-sm">
                  พร้อมจะรับฟังคุณด้วยความเข้าใจ
                  และอยู่เคียงข้างคุณในทุกการเปลี่ยนแปลง
                </span>
              </div>
              <div className="flex items-center gap-2 bg-text_white px-4 rounded-3xl text-text_black">
                <img src={flower} alt="flower" />
                <span className="text-sm">
                  ไม่เพียงแต่เป็นผู้รับฟัง
                  แต่จะช่วยให้คุณเติบโตขึ้นได้จากพลังที่มีอยู่ในตัวของคุณ
                </span>
              </div>
              <div className="h-full w-1/2">
                <Carousel
                  afterChange={onChange}
                  arrows={true}
                  prevArrow={<img src={al} alt="prev" />}
                  nextArrow={<img src={ar} alt="next" />}
                  slidesToShow={3}
                  autoplay
                  autoplaySpeed={2000}
                >
                  {therapistMock.map((v) => (
                    <Image
                      src={v.image}
                      alt="therappist"
                      preview={false}
                      key={v.id}
                    />
                  ))}
                </Carousel>
              </div>
            </div>
          </motion.section>
          <section
            className="bg-text_white overflow-x-hidden flex flex-col gap-4"
            id="howto"
          >
            <h1 className="title text-center w-full mt-12">ปรึกษาเลย</h1>
            <h4 className="text-2xl text-center w-full mt-12">
              ง่ายๆ แค่ทำตามนี้
            </h4>
            <div className="flex px-16">
              <div className="w-full h-full flex flex-col items-center gap-2 p-2">
                <img src={mock1} alt="mock application" className="w-44 h-96" />
                <div className="m-3 flex h-12 w-12 items-center justify-center rounded-full bg-button_therapist text-text_white">
                  <p>1</p>
                </div>
                <div className="flex gap-2 items-center">
                  <span className="headText">ดาวน์โหลดแอป</span>
                  <img src={logo} alt="logo" className="w-12 h-10" />
                </div>
                <span className="text-center">
                  เริ่มต้นใช้งานด้วยการสมัครสมาชิก <br />{" "}
                  และเข้าสู่ระบบด้วยเบอร์โทรศัพท์
                </span>
                <button className="rounded-3xl border border-text_black bg-bg_app flex justify-center w-max px-4 py-1">
                  <div className="flex gap-2 items-center">
                    <span>ดาวน์โหลด</span>
                    <img
                      src={download_b_ic}
                      alt="mock application"
                      className="w-4 h-4 fill-black"
                    />
                    <Divider type="vertical" />
                    <div className="flex items-center gap-2">
                      <img
                        src={a_ic}
                        alt="mock application"
                        className="w-4 h-4 fill-black"
                      />
                      <img
                        src={g_ic}
                        alt="mock application"
                        className="w-4 h-4 fill-black"
                      />
                    </div>
                  </div>
                </button>
              </div>
              {howtoData.map((i) => (
                <div
                  className="w-full h-full flex flex-col items-center gap-2 p-2"
                  key={i.ic}
                >
                  <img
                    src={i.img}
                    alt="mock application"
                    className="w-44 h-96"
                  />
                  <div className="m-3 flex h-12 w-12 items-center justify-center rounded-full bg-button_therapist text-text_white">
                    <p>{i.id}</p>
                  </div>

                  <span className="headText">{i.title}</span>
                  <span className="text-center w-48">{i.desc}</span>
                </div>
              ))}
            </div>
          </section>
        </div>
        <footer
          className="bg-bg_green w-full h-full p-16 gap-6 flex flex-col"
          id="footer"
        >
          <div className="w-full flex justify-start">
            <img src={logo} alt="logo" className="w-[72] h-10" />
          </div>
          <div className="flex justify-between items-start">
            {/* footer1 */}
            <div className="flex flex-col justify-between h-full  gap-60 flex-1">
              <div className="flex flex-col gap-4">
                <span className="text-text_dark_green text-xl font-semibold">
                  บริษัท มายด์บอท จำกัด (สำนักงานใหญ่)
                </span>
                <span className="text-text_dark_green">
                  ตึก 667/15 อาคารอรรถบูรณ์ ชั้น 5 ห้อง 502/2 <br />
                  ถ.จรัญสนิทวงศ์ แขวงอรุณอมรินทร์ เขตบางกอกน้อย กรุงเทพฯ 10700
                </span>
              </div>
              <div className="flex gap-4">
                <Link
                  to="https://line.me/ti/p/%40/@mindbloom"
                  target="_blank"
                  className="bg-bg_app px-4 py-2 rounded-full border border-text_dark_green flex gap-2 hover:scale-105 hover:duration-700 ease-in-out"
                >
                  <img src={l_ic} alt="line" />
                  <span>MindBloom</span>
                </Link>
                <Link
                  to="https://www.facebook.com/mindbloom.th?mibextid=LQQJ4d"
                  target="_blank"
                  className="bg-bg_app px-4 py-2 rounded-full border border-text_dark_green flex gap-2 hover:scale-105 hover:duration-700 ease-in-out"
                >
                  <img src={f_ic} alt="facebook" />
                  <span>MindBloom</span>
                </Link>
              </div>
            </div>
            {/* footer2 */}
            <div className="flex justify-between w-full flex-1">
              <ul className="flex flex-col gap-4 text-text_dark_green">
                <Link to="#main">หน้าหลัก</Link>
                <Link to="#service">บริการของเรา</Link>
                <Link to="#garden">สวนมายด์บลูม</Link>
                <Link to="#therapist">นักจิตวิทยา</Link>
                <Link to="#howto">การใช้งานมายด์บลูม</Link>
                <Link to="#">ติดต่อเรา</Link>
              </ul>
              <ul className="flex flex-col gap-4 text-text_dark_green">
                <li>ข้อกำหนดละเงื่อนไข</li>
                <li>นโยบายความเป็นส่วนตัว</li>
                <li>นโยบายคุกกี้</li>
              </ul>
            </div>
            {/* footer3 */}
            <div className="flex flex-1 flex-col items-center gap-6">
              <img src={app_icon} alt="icon" className="w-24 h-28" />
              <button className="rounded-3xl border border-text_black bg-bg_app flex justify-center w-max px-4 py-1">
                <div className="flex gap-2 items-center">
                  <span>ดาวน์โหลด</span>
                  <img
                    src={download_b_ic}
                    alt="mock application"
                    className="w-4 h-4 fill-black"
                  />
                  <Divider type="vertical" />
                  <div className="flex items-center gap-2">
                    <img
                      src={a_ic}
                      alt="mock application"
                      className="w-4 h-4 fill-black"
                    />
                    <img
                      src={g_ic}
                      alt="mock application"
                      className="w-4 h-4 fill-black"
                    />
                  </div>
                </div>
              </button>
              <div className="flex gap-4">
                <button className="h-10 w-32 rounded-3xl flex flex-1 hover:scale-105 hover:duration-700 ease-in-out">
                  <img
                    src={a_store}
                    alt="app store"
                    className="w-full h-full"
                  />
                </button>
                <button className="h-10 w-32 rounded-3xl flex flex-1 hover:scale-105 hover:duration-700 ease-in-out">
                  <img
                    src={g_store}
                    alt="app store"
                    className="w-full h-full"
                  />
                </button>
              </div>
            </div>
          </div>
        </footer>
      </BrowserRouter>
    </>
  );
}

export default App;
